### justree

## Before run
1) run `composer install`
2) create database and import `src/migrations/tree.sql`
3) change `src/config/db.php` to database config
4) configure Web Server

## Nginx Config Example
```
server {
    listen 80;
    listen [::]:80;
    server_name $domen_name;
    root $path_to_project_dir/src;

    index index.php;

    location / {
        try_files $uri $uri/ /index.php?$query_string;
    }

    location = /favicon.ico { access_log off; log_not_found off; }
    location = /robots.txt  { access_log off; log_not_found off; }

    error_page 404 /index.php;

    location ~ \.php$ {
        fastcgi_pass unix:/var/run/php/php8.1-fpm.sock;
        fastcgi_param SCRIPT_FILENAME $realpath_root$fastcgi_script_name;
        include fastcgi_params;
    }

    location ~ /\.(?!well-known).* {
        deny all;
    }
}
```

