<?php

namespace Justree\Model;

use \Justree\Base\Core\Model;
class Tree extends Model
{
    public $columns = ['name', 'parent'];

    // todo: rewrite to transaction
    public function deleteAll(): bool
    {
        $children = Tree::all(['parent' => $this->id]);
        foreach ($children as $node) {
            if (!$node->deleteAll()) {
                return false;
            }
            if (!$node->delete()) {
                return false;
            }
        }

        return $this->delete();
    }
}