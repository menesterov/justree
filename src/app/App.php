<?php

namespace Justree;


use Justree\Base\Config;
use Justree\Base\Core\HTTP\Response;
use Justree\Base\Exceptions\WrongResponseException;
use Justree\Base\HTTP\Request;
use Justree\Base\Routers\APIRouter;
use Justree\Base\Routers\FileRouter;
use Justree\Base\Routers\SimpleWebRouter;
use Justree\Model\Tree;

class App
{
    public function __construct(private $config)
    {
        Config::createConfig($this->config);
        Config::add('baseDir', dirname(__DIR__ ));
    }

    public function run()
    {
        $request = new Request();

        if ($request->isScope('api')) {
            $router = new APIRouter($request);
        } elseif ($request->isScope('file')) {
            $router = new FileRouter($request);
        } else {
            $router = new SimpleWebRouter($request);
        }

        $controller = $router->getController();
        $action = $router->getAction();
        $response = $controller->$action($request);

        if ($response instanceof Response) {
            $response->output();
        } else {
            throw new WrongResponseException($response, $request);
        }
    }
}