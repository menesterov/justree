<?php

namespace Justree\Base\Exceptions;

class NotExistsException extends \Exception
{
    public function __construct($entity, $name)
    {
        \Exception::__construct(    ucfirst("$entity '$name' doesn't exist"));
    }
}