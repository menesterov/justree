<?php

namespace Justree\Base\Exceptions;

use Justree\Base\Core\HTTP\Request;

class WrongResponseException extends \Exception
{
    public function __construct(mixed $response, ?Request $request)
    {
        // todo: response for API and web
        if (empty($response)) {
            // http_redirect(404)
        }
        die('Wrong response');
    }
}