<?php

namespace Justree\Base\Routers;

use Justree\Base\Core\Router;

class APIRouter extends Router
{
    const CONTROLLER_PATH = 'Justree\\APIController\\';

    public function getControllerName(): string
    {
        $entityName = $this->request->getUri()[2];
        if (!empty($entityName)) {
            $this->controllerName = $entityName;
        }
        return parent::getControllerName();
    }

    public function getAction(): string
    {
        $path = $this->request->getUri();
        if (count($path) > 3 && !$this->request->get('id')) {
            $this->request->set('id', end($path));
        }
        return $this->request->getMethod();
    }
}