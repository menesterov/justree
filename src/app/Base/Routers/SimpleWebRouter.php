<?php

namespace Justree\Base\Routers;

use Justree\Base\Core\Router;

class SimpleWebRouter extends Router
{
    const CONTROLLER_PATH = 'Justree\\Controller\\';
    public function getControllerName(): string
    {
        $entityName = $this->request->getUri()[1] ?? 'home';
        $this->controllerName = !empty($entityName) ? $entityName : 'home';
        if (!class_exists($this->controllerName)) {
            $this->controllerName = 'home';
        }
        return parent::getControllerName();
    }

    public function getAction(): string
    {
        $actionIndex = 'home' == $this->controllerName ? 1 : 2;
        $actionName = $this->request->getUri()[$actionIndex] ?? 'index';
        $actionName = !empty($actionName) ? $actionName : 'index';
        return $actionName;
    }
}