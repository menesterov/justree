<?php

namespace Justree\Base\Routers;

use Justree\Base\Core\Router;

class FileRouter extends Router
{
    const CONTROLLER_PATH = 'Justree\\Base\\Controllers\\';

    public function getControllerName(): string
    {
        return 'FileController';
    }

    public function getAction(): string
    {
        return 'file';
    }
}