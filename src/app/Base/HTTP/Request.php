<?php

namespace Justree\Base\HTTP;

use Justree\Base\Core\Contracts\Scopable;
use Justree\Base\Core\HTTP\Request as CoreRequest;

class Request extends CoreRequest implements Scopable
{
    const DEFAULT_SCOPE = 'web';

    public function getScope(): string
    {
        $URIParts = $this->getUri();
        if (strpos(array_pop($URIParts), '.') > 0) {
            return 'file';
        }
        if(sizeof($URIParts) > 1 &&  $URIParts[1] == 'api') {
            return 'api';
        }

        return static::DEFAULT_SCOPE;
    }
    public function isScope(string $scopeName): bool
    {
        return $this->getScope() == $scopeName;
    }
}