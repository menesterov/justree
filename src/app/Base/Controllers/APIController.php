<?php

namespace Justree\Base\Controllers;

use Justree\Base\Core\Controller;
use Justree\Base\Core\HTTP\Response;

class APIController extends Controller
{
    public function json($data, $code = 200): Response
    {
        $response = $this->createResponse($code);
        $response->addHeader('Content-Type', 'application/json');
        $response->setResult(json_encode($data));

        return $response;
    }

}