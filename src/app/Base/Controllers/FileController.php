<?php

namespace Justree\Base\Controllers;

use Justree\Base\Config;
use Justree\Base\Core\Controller;
use Justree\Base\Core\HTTP\Response;
use Justree\Base\HTTP\Request;

class FileController extends Controller
{
    const PUBLIC_DIR = '/public';
    const CONTENT_TYPES = [
        'js'    => 'application/javascript',
        'css'   => 'text/css',
    ];
    public function file(Request $request): Response
    {
        $filePath = Config::baseDir(). self::PUBLIC_DIR . $request->getUriString();

        if (file_exists($filePath)){
            $extension = pathinfo($filePath, PATHINFO_EXTENSION);
            $contentType = self::CONTENT_TYPES[$extension] ?? mime_content_type($filePath);
            $response = $this->createResponse();
            $response->addHeader('Content-Type', $contentType);
            $response->setResult(file_get_contents($filePath));
        } else {
            $response = $this->createResponse(404);
        }
        return $response;
    }
}