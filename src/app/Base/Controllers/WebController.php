<?php

namespace Justree\Base\Controllers;

use Justree\Base\Config;
use Justree\Base\Core\Controller;
use Justree\Base\Core\HTTP\Response;
use Justree\Base\Core\View;
use Justree\Base\Exceptions\NotExistsException;

class WebController extends Controller
{
    public function render($template, $data = []): Response
    {
        $code = 200;
        $result = '';
        try {
            $view = new View($template);
            $result = $view->render($data);
        } catch (NotExistsException $exception) {

            $view = new View('404');
            if (Config::isDevMode()) {
                $data['message'] = $exception->getMessage();
            }
            $result = $view->render($data);
        } catch (\Exception $exception) {
            $code = 404;
            if (Config::isDevMode()) {
                $result = $exception->getMessage();
            }
        }
        $response = $this->createResponse($code);
        $response->addHeader('Content-Type', 'text/html');
        $response->setResult($result);
        return $response;
    }

    public function __call($actionName, $data): Response
    {
        $response = $this->createResponse();
        $response->addHeader('Content-Type', 'text/html');

        $view = new View('404', ['message' => "Page not found"]);
        $response->setResult($view->render($data));

        return $response;
    }
}