<?php

namespace Justree\Base;

class Config
{
    static $config = [];

    public static function createConfig(array $data)
    {
        self::$config =  $data;
    }

    public static function add($key, $value)
    {
        self::$config[$key] = $value;
    }

    public static function get($name)
    {
        if (isset(self::$config[$name])) {
            return self::$config[$name];
        }
        return null;
    }

    public static function isDevMode(): bool
    {
        return strtolower(self::app('mode') == 'dev');
    }

    public static function __callStatic($name, $argList)
    {
        $result = self::get($name);
        $argIndex = 0;
        while (is_array($result) && isset($argList[$argIndex])) {
            if (isset($result[$argList[$argIndex]])) {
                $result = $result[$argList[$argIndex]];
                $argIndex++;
            }
        }
        return $result;
    }
}