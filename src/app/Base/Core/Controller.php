<?php

namespace Justree\Base\Core;

use Justree\Base\Core\HTTP\Response;

class Controller
{
    public function createResponse($code = 200): Response
    {
        $response = new Response();
        $response->setCode($code);
        return $response;
    }
    public function message(string $text): Response
    {
        $response = $this->createResponse();
        $response->addHeader('Content-Type', 'text/plain');
        $response->setResult($text);

        return $response;
    }
}