<?php

namespace Justree\Base\Core\HTTP;


use Justree\Base\Traits\ParamsGetter;

class Request
{
    protected $method;
    protected $params;
    protected $pathString;

    public function __construct()
    {
        $this->method = strtolower($_SERVER['REQUEST_METHOD']);
        parse_str(file_get_contents('php://input'),$this->params);
        $this->pathString = explode('?', $_SERVER['REQUEST_URI'])[0];
    }

    public function getMethod(): string
    {
        return $this->method;
    }
    public function get($name, $default = null): mixed
    {
        if (isset($this->params[$name])) {
            return  $this->params[$name];
        } else {
            return $default;
        }
    }

    public function set($name, $value)
    {
        $this->params[$name] = $value;
    }

    public function all(): array
    {
        return $this->params;
    }

    public function getUriString(): string
    {
        return $this->pathString;
    }

    public function getUri(): array
    {
        return explode('/', $this->getUriString());
    }
}