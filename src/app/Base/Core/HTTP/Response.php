<?php

namespace Justree\Base\Core\HTTP;

class Response
{
    protected $headers = [];
    protected $code;

    protected $result = '';


    public function addHeader($title, $value)
    {
        $this->headers[$title] = $value;
    }

    public function setResult($result)
    {
        $this->result = $result;
    }

    public function setCode($code)
    {
        $this->code = $code;
    }

    public function printResult()
    {
        print($this->result);
    }

    public function output()
    {
        ignore_user_abort(true);
        ob_start();

        $this->printResult();
        $contentLength = ob_get_length();

        http_response_code($this->code);
        foreach ($this->headers as $title => $value) {
            header("$title: $value");
        }
        header("Content-Length: $contentLength");

        ob_end_flush();
        ob_flush();
        flush();
    }

}
