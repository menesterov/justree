<?php

namespace Justree\Base\Core;

use Justree\Base\Core\HTTP\Request;

abstract class Router
{
    const CONTROLLER_PATH = self::CONTROLLER_PATH;
    protected $controllerName = '';

    abstract public function getAction(): string;

    public function __construct(protected Request $request)
    {
    }

    protected function getControllerPath(): string
    {
        return static::CONTROLLER_PATH;
    }

    public function getControllerName(): string
    {
        return ucfirst($this->controllerName) . 'Controller';
    }

    public function getController(): Controller
    {
        $controller = $this->getControllerPath() . $this->getControllerName();
        return new $controller();
    }
}