<?php

namespace Justree\Base\Core;

use Justree\Base\Exceptions\NotExistsException;

class View
{
    const DEFAULT_VIEW_PATH = '/../../../views/';
    const DEFAULT_EXTENSION = 'php';

    public function __construct(protected $name)
    {
    }

    public function getFileName()
    {
        $namePath = str_replace('.', '/', $this->name);
        return __DIR__ . static::DEFAULT_VIEW_PATH . $namePath . '.' . static::DEFAULT_EXTENSION;
    }
    public function render(array $variables)
    {
        if (!$this->isExists()) {
            throw new NotExistsException('view', $this->name);
        }

        extract( $variables);

        ob_start();
        include $this->getFileName();
        return ob_get_clean();
    }

    public function isExists()
    {
        return file_exists($this->getFileName());
    }
}