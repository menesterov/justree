<?php

namespace Justree\Base\Core;

class Model
{
    public $columns = ['id'];
    public $id;

    public function __construct()
    {
        // remove id from columns
        $this->columns = array_diff($this->columns, ['id']);

        //init columns as properties
        foreach ($this->columns as $propName) {
            $this->$propName = null;
        }
    }

    public static function getTable(): string
    {
        $classPath = explode('\\', static::class);
        return strtolower(array_pop($classPath));
    }

    protected function set(array $data): self
    {
        foreach ($this->columns as $propName) {
            if (isset($data[$propName])) {
                $this->$propName = $data[$propName];
            }
        }
        return $this;
    }

    public static function all(array $conditions = [], int $limit = null): array
    {
        $sql = "SELECT * FROM " . static::getTable();
        if (!empty($conditions)) {
            $condList = [];
            foreach ($conditions as $key => $value) {
                $condList[] = "`$key`=:$key";
            }
            $sql .= " WHERE " . implode(' AND ', $condList);
        }
        if (isset($limit)) {
            $sql .= " LIMIT $limit";
        }

        $collection = [];
        foreach (DB::getInstance()->getAll($sql, $conditions) as $record) {
            $item = new static();
            $item->set($record);
            $item->id = $record['id'];
            $collection[] = $item;
        }
        return $collection;
    }

    public static function one(array $conditions = []): self
    {
        return self::all($conditions, 1)[0];
    }

    public static function get(int $id): self
    {
        return self::one(['id' => $id]);
    }

    protected function insertSelf(): bool
    {
        $bindMap = [];
        foreach ($this->columns as $propName) {
            if (isset($this->$propName)) {
                $bindMap[$propName] = $this->$propName;
            }
        }
        $sql = "INSERT INTO " . static::getTable()
            . "(" . implode(',', array_keys($bindMap)) . ")"
            . " VALUES(:" . implode(',:', array_keys($bindMap)) . ")"
        ;

        $result = DB::getInstance()->bind($sql, $bindMap)->execute();
        if ($result) {
            $this->id = DB::getInstance()->lastInsertId();
        }
        return $result;
    }

    protected function updateSelf(): bool
    {
        $sql = "UPDATE " . static::getTable();
        $updateList = [];
        $bindMap = [];
        foreach ($this->columns as $propName) {
            if (isset($this->$propName)) {
                $bindMap[$propName] = $this->$propName;
                $updateList[] = "`$propName`=:$propName";
            }
        }
        $sql .= " SET " . implode(', ', $updateList);
        $sql .= " WHERE `id`=" . $this->id;

        $result = DB::getInstance()->bind($sql, $bindMap)->execute();
        return $result;
    }

    public function save(): bool
    {
        if ($this->id) {
            return $this->updateSelf();
        }
        return $this->insertSelf();
    }

    public function delete(): bool
    {
        $sql = "DELETE FROM " . static::getTable();
        $sql .= " WHERE `id`=" . $this->id;

        $result = DB::getInstance()->bind($sql)->execute();
        return $result;
    }
}