<?php

namespace Justree\Base\Core\Contracts;

interface Scopable
{
    public function getScope(): string;
    public function isScope(string $scopeName): bool;
}