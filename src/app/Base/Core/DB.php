<?php

namespace Justree\Base\Core;

use Justree\Base\Config;

class DB extends \PDO
{
    static private $instance;

    private function __construct()
    {
        $type = Config::db('type') ?? 'mysql';
        $port = Config::db('port') ?? ('pgsql' == $type ? 5432 : 3306);
        $host = Config::db('host') ?? 'localhost';
        $name = Config::db('dbname') ?? Config::db('name')  ?? Config::app('name');
        $user = Config::db('username') ?? Config::db('user') ?? 'user';
        $pass = Config::db('password') ?? Config::db('pass') ?? '';
        parent::__construct("$type:host=$host;port=$port;dbname=$name", $user, $pass);
    }

    public function __destruct()
    {
        self::$instance = null;
    }

    public static function getInstance()
    {
        if (!self::$instance instanceof self) {
            self::$instance = new self();
        }
        return self::$instance;
    }

    public function bind(string $sql, array $params = []): \PDOStatement
    {
        $q = $this->prepare($sql);
        if (!empty($params)) {
            foreach ($params as $key => $value) {
                $q->bindValue(":$key", $value);
            }
        }
        return $q;
    }

    public function getAll(string $sql, array $params = []): array
    {
        $q = $this->bind($sql, $params);
        $q->execute();
        return $q->fetchAll(\PDO::FETCH_ASSOC);
    }

    public function getRow(string $sql, array $params = []): array
    {
        $result = $this->getAll($sql, $params);
        return $result[0] ?? [];
    }

    public function getOne(string $sql, array $params = [])
    {
        $result = $this->getRow($sql, $params);
        return $result[0] ?? null;
    }

}