<?php

namespace Justree\Controller;

use Justree\Base\Controllers\WebController;
use Justree\Base\Core\HTTP\Request;
use Justree\Model\Tree;

class HomeController extends WebController
{
    public function index(Request $request)
    {
        return $this->render('tree', [
            'trees' => Tree::all()
        ]);
    }
}