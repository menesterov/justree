<?php

namespace Justree\APIController;

use Justree\Base\Controllers\APIController;
use Justree\Base\Core\HTTP\Request;

class TestController extends APIController
{
    public function get(Request $request)
    {
        return $this->json('ok');
    }
}