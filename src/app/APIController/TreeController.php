<?php

namespace Justree\APIController;

use Justree\Base\Controllers\APIController;
use Justree\Base\Core\HTTP\Request;
use Justree\Model\Tree;

class TreeController extends APIController
{
    public function get(Request $request)
    {
        if ($request->get('id')) {
            $result = Tree::get($request->get('id'));
        } elseif ($request->get('parent')) {
            $result = Tree::all(['parent' => $request->get('parent')]);
        } else {
            $result = Tree::all();
        }

        return $this->json($result);
    }

    public function post(Request $request)
    {
        $node = new Tree();
        if ($request->get('name')) {
            $node->name = $request->get('name');
        } else {
            return $this->json(['message' => 'Name is empty', 'data' => $request->all()], 400);
        }
        if ($request->get('parent')) {
            $node->parent = $request->get('parent');
        }
        if ($node->save()) {
            return $this->json($node);
        } else {
            return $this->json(['message' => 'Save node error'], 500);
        }
    }

    public function delete(Request $request)
    {
        if ($request->get('id')) {
            $node = Tree::get($request->get('id'));
        } else {
            return $this->json(['message' => 'ID is empty', 'data' => $request->all()], 400);
        }

        if ($node->deleteAll()) {
            return $this->json(['message' => 'Success'], 200);
        } else {
            return $this->json(['message' => 'Delete node error'], 500);
        }
    }

    public function patch(Request $request)
    {
        if ($request->get('id')) {
            $node = Tree::get($request->get('id'));
        } else {
            return $this->json(['message' => 'ID is empty', 'data' => $request->all()], 400);
        }
        if ($request->get('name')) {
            $node->name = $request->get('name');
        }
        if ($node->save()) {
            return $this->json($node);
        } else {
            return $this->json(['message' => 'Save node error'], 500);
        }
    }
}