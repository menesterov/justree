<?php

require __DIR__ . '/../vendor/autoload.php';
require __DIR__ . '/config/config.php';

if (isset($config['app']['mode']) && $config['app']['mode'] == 'dev') {
    ini_set("display_errors", "1");
    error_reporting(E_ALL & ~E_NOTICE);
}

$app = new \Justree\App($config);
$app->run();
