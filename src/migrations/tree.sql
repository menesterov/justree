CREATE TABLE tree (
    `id`        BIGINT UNSIGNED AUTO_INCREMENT,
    `name`      VARCHAR(255),
    `parent`    BIGINT UNSIGNED DEFAULT NULL,
    PRIMARY KEY (`id`),
    UNIQUE (`name`),
    FOREIGN KEY (`parent`) REFERENCES tree(`id`)
) CHARACTER SET utf8 COLLATE utf8_general_ci;