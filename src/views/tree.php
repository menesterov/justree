<html>
<?php include('header.php')?>
<script src="/js/justree.js"></script>
<body>
    <div class="container">
        <div class="row">
            <h1>Just a Tree</h1>
        </div>
        <div class="row">
            <ul class="container">
                <template id="tree">
                    <li class="row" data-id="$id">
                        <div class="container">
                                    <button type="button" onclick="toggleTree($id)" class="arrow arrow-bottom up btn btn-sm btn-outline-primary">
                                        <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-arrow-down-right" viewBox="0 0 16 16">
                                            <path fill-rule="evenodd" d="M14 13.5a.5.5 0 0 1-.5.5h-6a.5.5 0 0 1 0-1h4.793L2.146 2.854a.5.5 0 1 1 .708-.708L13 12.293V7.5a.5.5 0 0 1 1 0v6z"/>
                                        </svg>
                                    </button>
                                    <span onclick="changeName($id, '$name')"><b>$name</b></span>
                                    <button type="button" onclick="appendToNode($id)" class="btn btn-sm btn-outline-success">+</button>
                                    <button type="button" onclick="removeNode($id, '$name')" class="btn btn-sm btn-outline-danger">-</button>
                            <ul class="col children">$children</ul>
                        </div>
                    </li>
                </template>
            </ul>
        </div>
    </div>
</body>
</html>

<script>
    $(document).ready(()=>{
        const tree = TreeFactory.create(<?= json_encode($trees) ?>);
        tree.name = 'Root';
        tree.draw();
    });

    function toggleTree(id)
    {
        const container = $('[data-id="'+id+'"]');
        container.find('.children').first().toggle();
        let arrow = container.find('.arrow').first();
        if (arrow.hasClass('arrow-right')) {
            arrow.removeClass('arrow-right');
            arrow.addClass('arrow-bottom');
            arrow.html('<svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-arrow-down-right" viewBox="0 0 16 16"><path fill-rule="evenodd" d="M14 13.5a.5.5 0 0 1-.5.5h-6a.5.5 0 0 1 0-1h4.793L2.146 2.854a.5.5 0 1 1 .708-.708L13 12.293V7.5a.5.5 0 0 1 1 0v6z"/></svg>');
        } else {
            arrow.removeClass('arrow-bottom');
            arrow.addClass('arrow-right');
            arrow.html('<svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-arrow-right" viewBox="0 0 16 16"><path fill-rule="evenodd" d="M1 8a.5.5 0 0 1 .5-.5h11.793l-3.147-3.146a.5.5 0 0 1 .708-.708l4 4a.5.5 0 0 1 0 .708l-4 4a.5.5 0 0 1-.708-.708L13.293 8.5H1.5A.5.5 0 0 1 1 8z"/> </svg>');
        }
    }

    function appendToNode(nodeId)
    {
        let name = prompt("New node name:");
        $.ajax({
            type: "POST",
            url: "/api/tree",
            data: {parent: nodeId, name: name}
        }).fail(function(result) {
            if (result.responseJSON.message) {
                alert(result.responseJSON.message);
            } else {
                alert("Can't create node by server error");
            }
        }).done(function(result) {
            if (result.id) {
                Tree.getNode(nodeId).appendNode(result.id, result.name).draw();
            }
        });
    }

    function removeNode(id, name)
    {
        if(!id) return;
        let answer = confirm("Node - " + name + " will be deleted. Are you sure?");
        if (answer) {
            $.ajax({
                type: "DELETE",
                url: "/api/tree/" + id,
            }).fail(function(result) {
                if (result.responseJSON.message) {
                    alert(result.responseJSON.message);
                } else {
                    alert("Can't create node");
                }
            }).done(function(result) {
                Tree.removeNodeAndChildren(id);
            });
        }
    }

    function changeName(id, name)
    {
        let newName = prompt("Edit node name:", name);
        if (!newName || newName == name) return;
        $.ajax({
            type: "PATCH",
            url: "/api/tree/"+id,
            data: {name: newName}
        }).fail(function(result) {
            if (result.responseJSON.message) {
                alert(result.responseJSON.message);
            } else {
                alert("Can't change name");
            }
        }).done(function(result) {
            if (result.name) {
                const node = Tree.getNode(id);
                node.name = result.name
                node.draw();
            }
        });
    }
</script>
