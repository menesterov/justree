class Tree {
    // tree
    static #nodes = [];
    constructor(id=0, name='', parent=0)
    {
        this.id = + id ?? 0;
        this.name = name ?? '';
        this.parent = + parent ?? 0;
        Tree.#nodes.push(this);
    }

    static addNode(id, name, parent)
    {
        const node = new Tree(id, name, parent);
        return node;
    }

    static getNode(id)
    {
        return Tree.#nodes.find((item) => item.id === id);
    }

    getChildNodes()
    {
        return Tree.#nodes.filter((item) => item.parent == this.id && item.parent != item.id)
    }

    appendNode(id, name)
    {
        const item = Tree.addNode(id, name, this.id);
        return item;
    }

    static removeNodeAndChildren(id)
    {
        let node = Tree.getNode(id);
        node.getChildNodes().forEach((node) => node.remove);
        node.remove();
    }

    remove()
    {
        this.erase();
        Tree.#nodes = Tree.#nodes.filter((item) => item.id != this.id);
    }

    getHtml(template)
    {
        let html = template;
        let nodeHtml = '';
        this.getChildNodes().forEach((node) => {
            nodeHtml += node.getHtml(template)
        });
        html = html.replace('$children', nodeHtml);
        html = html.replaceAll('$id', this.id);
        html = html.replaceAll('$name', this.name);
        return html;
    }

    static getTemplate()
    {
        return $('template#tree');
    }

    draw()
    {
        if (this.id) this.erase();
        let container = Tree.getTemplate().parent();
        if ($('[data-id="'+this.parent+'"]').length) {
            container = $('[data-id="'+this.parent+'"]').find('.children').first()
        }
        container.append(this.getHtml(Tree.getTemplate().html()));
    }

    erase()
    {
        $('[data-id="'+this.id+'"]').remove();
    }
}

class TreeFactory {
    static create(listOfObjects)
    {
        const tree = new Tree();
        listOfObjects.forEach((item) => Tree.addNode(item.id, item.name, item.parent))
        return tree;
    }
}
